package com.example.library;

import java.util.List;

/**
 * Created by AleM on 12/01/15.
 */

//This class is called by BookController and goes to BookHibernate class
public interface BookDAO {

	//public Book getBook(Long id);
	//public List<Book> getBooks(String[] queryParamNames, Object[] queryParamValues);
	//public List<Book> getBooks(String query);

	public Book create(Book book);

	public List<Book> getBooks();
	public List getBooksByAuthor(String author);
	public List<Book> getBooksByTitle(String title);
	
	public void updateBook(Book book);
	
	public void deleteBook(Long id);
}
