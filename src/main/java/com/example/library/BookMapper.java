package com.example.library;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class BookMapper implements RowMapper<Book> {

	@Override
	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Book(rs.getLong("id"), rs.getString("isbn"), rs.getString("title"), rs.getString("author"), rs.getInt("numSells"));
	}
}
