package com.example.library;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Created by AleM on 12/01/15.
 */

//@Repository explains that this class interact directly with DB.
@Repository
public class BookHibernate implements BookDAO {

    private SessionFactory sessionFactory;

    //@Inject explains that when we interact with DB, we need to create a session. This command does it for us.
    @Inject
    public BookHibernate(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;		 //<co id="co_InjectSessionFactory"/>
    }

    private Session currentSession() {
        return sessionFactory.getCurrentSession();//<co id="co_RetrieveCurrentSession"/>
    }

    public List<Book> getBooks() {
        return  currentSession()
                .createCriteria(Book.class).list();
    }

    public List<Book> getBooksByAuthor(String author) {
        return currentSession()
                .createCriteria(Book.class)
                .add(Restrictions.like("author", "%" + author + "%"))
                .list();
    }

    public List<Book> getBooksByTitle(String title) {
        return currentSession()
                .createCriteria(Book.class)
                .add(Restrictions.like("title", "%" + title + "%"))
                .list();
    }

    public Book create(Book book) {
        Serializable id = currentSession().save(book);  //<co id="co_UseCurrentSession"/>

        //This new book is for show the data of the book correctly.
        return new Book((Long) id,
                book.getIsbn(),
                book.getAuthor(),
                book.getTitle(),
                book.getNumSells());
    }

    //saveOrUpdate, checks if the book exists. If exists, it update it. But if not exists, it creates the book.
    @Override
    public void updateBook(Book book) {
        currentSession().saveOrUpdate(book);
    }

    //delete only accepts an object. So we have to obtain the book first and then delete it.
    @Override
    public void deleteBook(Long id) {
        Book book = (Book) currentSession().get(Book.class, id);
        currentSession().delete(book);
    }
}
