package com.example.library;

import javax.persistence.*;

/**
 * Created by AleM on 12/01/15.
 */

//Annotation that explain that this class is an entity of HBN
@Entity(name = "books")
public class Book {

    //@GeneratedValue explains that this is the primary key
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column(name="isbn")
    private String isbn;

    @Column(name="author")
    private String author;

    @Column(name="title")
    private String title;

    @Column(name="numSells")
    private int numSells;

    //private String publishedDate;

    //Constructors
    public Book() {
    }

    public Book(long id, String isbn, String title, String author, int numSells) {
        this.id = id;
        this.isbn = isbn;
        this.author = author;
        this.title = title;
        this.numSells = numSells;
    }

    //Getters & Setters
    public long getId() {
        return id;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public int getNumSells() {
        return numSells;
    }

    /*public String getPublishedDate() {
        return publishedDate;
    }

    public void setPublishedDate(String publishedDate) {
        this.publishedDate = publishedDate;
    }*/

    public void setId(long id) {
        this.id = id;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setNumSells(int numSells) {
        this.numSells = numSells;
    }

    //Method to show the class in the console correctly.
    @Override
    public String toString() {
        return "\n" +
                " Book [id=" + id + ", "
                + (isbn != null ? "isbn=" + isbn + ", " : "")
                + (author != null ? "author=" + author + ", " : "")
                + (title != null ? "title=" + title + ", " : "")
                + "numSells=" + numSells
                + ", " + "]";
    }
}