package com.example.library;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * Created by AleM on 12/01/15.
 */

@ComponentScan
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
@ImportResource("classpath:META-INF/spring/applicationContext.xml")

public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
