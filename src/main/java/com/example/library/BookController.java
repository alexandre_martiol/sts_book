package com.example.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by AleM on 12/01/15.
 */

//@RequestMapping explains that in the URL, all requests with "/books" will come here
@RequestMapping(value = "/books")
@RestController

//@Transactional starts a transaction in every method of this class.
@Transactional
public class BookController {
	@Autowired
	BookDAO bookDAO;
	Book book;
    
	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
    public List<Book> searchTitleAndAuthor(
			@RequestParam(value = "author", required = false) String author,
			@RequestParam(value = "title", required = false) String title) {

		/*
		//The idea is to create the query depending on the params that user has entered
		ArrayList <String> params = new ArrayList<String>();
		ArrayList <Object> values = new ArrayList<Object>();
		String query = "SELECT * FROM books WHERE "; 
		
        if (author != null && author != "") {
        	params.add("author");
        	values.add(author);
        }
        
        if (title != null && title != "") {
        	params.add("title");
        	values.add(title);
        }
        
        for (int i = 0; i < params.size(); i++) {
        	if (i == 0) {
        		query += params.get(i) + " LIKE '%" + values.get(i) + "%'";
        	}
        	
        	else {
        		query += " AND " + params.get(i) + " LIKE '%" + values.get(i) + "%'";
        	}
        }
        
        if (params.size() == 0) {
        	query = "SELECT * FROM books";
        }
        
        return bookDAO.getBooks(query);   */

		if (author != null && author != "") {
			return bookDAO.getBooksByAuthor(author);
		}

		else if (title != null && title != "") {
			return bookDAO.getBooksByTitle(title);
		}

		else {
			return bookDAO.getBooks();
		}
    }

	//@RequestBody expects a JSON class code in the request
	@RequestMapping(method = RequestMethod.POST)
	public void createBook(@RequestBody Book book) {
		bookDAO.create(book);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public void updateBook(@RequestBody Book book) {
		bookDAO.updateBook(book);
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public void deleteBook(@RequestParam(value = "id", required = true) Long id) {
		bookDAO.deleteBook(id);
	}
}
