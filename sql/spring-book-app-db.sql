# Dumping database structure for spring_bank_app_db
DROP DATABASE IF EXISTS `bookdatabase`;
CREATE DATABASE IF NOT EXISTS `bookdatabase` DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;
USE `bookdatabase`;


# Dumping structure for table book
DROP TABLE IF EXISTS `books`;
CREATE TABLE IF NOT EXISTS `books` (
  id serial,
  isbn varchar(255) UNIQUE,
  title varchar(255),
  author varchar(255),
  numSells int(11),
  PRIMARY KEY(`id`)
  ) ENGINE=InnoDB DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci COMMENT='Contains information about books';

# INSERT BOOKS
INSERT INTO books(isbn, title, author, numSells) values("9788441531482", "IOS 5 (PROGRAMACION)", "Rob Napier", 5000);
INSERT INTO books(isbn, title, author, numSells) values("9788441531470",	"Android 3 (PROGRAMACION)", "John Doe", 10000);
INSERT INTO books(isbn, title, author, numSells) values("9788441531450",	"Spring (PROGRAMACION)", "Jane Doe", 50000);
INSERT INTO books(isbn, title, author, numSells) values("9788441531460", "IOS 6 (PROGRAMACION)", "Rob Napier", 9000);